function get_info_on_var(variable) {
    var rel_meta = meta_data.find(function(d) {
        return d.Variabele == variable;
    })

    var label = rel_meta['Label_1'];
    var definition = rel_meta['Definition'];

    return [label, definition]
}

function updateArea(selectObject) {
    selected_area = selectObject.value;
    updatePlot();
};

function updatePlot() {
    var fetch_url = "/d3_plot_data?area_name=" + selected_area;
    fetch(fetch_url)
        .then(function(response) { return response.json(); })
        .then((data) => {
            plot_data = data;
            createNewChart();
    });
}

function makeLegend(map, color_map) {

    // Make legend's labels

    legend_group.selectAll("text")
        .data(
            map.leaves()
        )
        .enter()
        .append("text")
        .attr("x", function (d,i) {return 70 + chart_width; })
        .attr("y", function (d,i) {return 100 + i*30 + 4; })
        .text(function (d) {return d.data.name;})
        .attr("fill", "black")
        .attr("font-size", "10px")
        .on("mouseover", function (d) {

            // Select respective rectangle in treemap and make it opaque to highlight it, also add a frame
            getFromGroup(chart_group, '.bar', d)
                .style("fill-opacity", 0.5)
                .style("stroke-width", "2px")
                .style("stroke", "black");

            // Make text bold
            d3.select(this).style("font-weight", "bold");

        })
        .on("mouseout", function (d) {

            // Select respective rectangle in treemap and make it back to opacity 1 and remove frame
            getFromGroup(chart_group, '.bar', d)
                .style("fill-opacity", 1)
                .style("stroke", "none");

            // Turn text normal from bold
            d3.select(this).style("font-weight", "normal");
        });

    // Make legend's circles

    legend_group.selectAll("circle")
        .data(
            map.leaves()
        )
        .enter()
        .append("circle")
    // location and size
        .attr("cx", function (d,i) {return 50 + chart_width; })
        .attr("cy", function (d,i) {return 100 + i*30; })
        .attr("r", 8)
    // color
        .attr("fill", function (d) {return color_map(d.data.name);})
    // events
        .on("mouseover", function (d) {

            // Select respective rectangle in treemap and make it opaque to highlight it, also add a frame
            getFromGroup(chart_group, '.bar', d)
                .style("fill-opacity", 0.5)
                .style("stroke-width", "2px")
                .style("stroke", "black");

            // Select respective text in legend and make it bold
            getFromGroup(legend_group, "text", d)
                .style("font-weight", "bold");


        })
        .on("mouseout", function (d) {

            // Select respective rectangle in treemap and make it back to opacity 1 and remove frame
            getFromGroup(chart_group, '.bar', d)
                .style("fill-opacity", 1)
                .style("stroke", "none");

            // Select respective text in legend and make it back to normal (from bold)
            getFromGroup(legend_group, "text", d)
                .style("font-weight", "normal");
        });
}

function makeTreeMap(data) {

    if (data==null){
        return null;
    }

    hierarchyData = {
        name : selected_area,
        value : 0,
        children : []
    };

    d3.entries(data[0])
        .forEach(
            function(d){
                hierarchyData.children.push(
                    {
                        name : d.key,
                        key : d.key,
                        value : d.value
                    }
                );

                hierarchyData.value += d.value;
            }
        );

    return d3.treemap()
        .size([chart_width, chart_height])
        .padding(1)
        .round(false)
    (d3.hierarchy(hierarchyData));

}

function getFromGroup (group, type, d) {
    return group.selectAll(type)
        .filter(function(d2){
            return d2.data.name == d.data.name;
        });
}

function createNewChart() {

    var map = makeTreeMap(plot_data);

    // For debugging (show full map in console)
    console.log("Full map: ", map);

    // Create color map (assign color to each label)
    var color_map = d3.scaleOrdinal()
        .domain(Object.keys(plot_data[0]))
        .range(d3.schemeDark2);

    // Make legend for the first time
    if (! LEGENDMADE) {
        makeLegend(map, color_map);
        LEGENDMADE = true;
    }

    // Make treemap's rectangles

    var treemap_chart = chart_group.selectAll(".bar")
        .data(
            map.leaves()
        );

    treemap_chart
        .enter()
        .append("rect")
        .merge(treemap_chart)
    // css style
        .attr("class", "bar")
        .attr("fill", function (d) {return color_map(d.data.name);})
    // events
        .on("mouseover", function(d, i) {
            var x_var = d.data.name;
            var value = d.data.value;
            var info = get_info_on_var(x_var);
            var label = info[0];
            var definition = info[1];

            displayTooltip("<b>Name: </b>" + x_var + "<br /><b>Variable: </b>" + label + "<br /><b>Percentage: </b>" + 
                           value + "%<br /><b>Explanation: </b>" + definition);

            // Get corresponding text in legend and make it bold
            getFromGroup(legend_group, "text", d)
                .style("font-weight", "bold");

            // Make rectangle opaque to highlight it
            d3.select(this)
                .style("fill-opacity", 0.5);
        })
        .on("mousemove", function(d, i) {
            var x_var = d.data.name;
            var value = d.data.value;
            var info = get_info_on_var(x_var);
            var label = info[0];
            var definition = info[1];

            displayTooltip("<b>Name: </b>" + x_var + "<br /><b>Variable: </b>" + label + "<br /><b>Percentage: </b>" + 
                           value + "%<br /><b>Explanation: </b>" + definition);

        })
        .on("mouseout", function(d) {

            hideTooltip();

            // Get corresponding text in legend and make it normal (from bold)
            getFromGroup(legend_group, "text", d)
                .style("font-weight", "normal");

            // Make opacity back to 1
            d3.select(this)
                .style("fill-opacity", 1);
        })
    // transition
        .transition()
        .duration(1500)
    // new location and size
        .attr("x", function (d) { return d.x0; })
        .attr("y", function (d) { return d.y0; })
        .attr("width", function(d) { return d.x1 - d.x0; })
        .attr("height", function (d) { return d.y1 - d.y0; });

    treemap_chart
        .exit()
        .remove();

    // Make treemap's labels

    var treemap_labels = chart_group.selectAll("text")
        .data(
            map.leaves()
        );

    treemap_labels
        .enter()
        .append("text")
        .merge(treemap_labels)
    // general settings
        .style("text-anchor", "middle")
    // style
        .attr("fill", "black")
        .style("text-orientation", "upright")
        .attr("font-size", "10px")
    // add underlying rectangle's label
        .text(function (d) { return d.data.name;})
    // transiton
        .transition()
        .duration(1500)
    // new location and vertical/horizontal writing
        .attr("x", function (d) { return d.x0 + (d.x1 - d.x0)/2;})
        .attr("y", function (d) { return d.y0 + (d.y1 - d.y0)/2;})
        .style("writing-mode", function (d) {return ((d.y1 - d.y0) > (d.x1 - d.x0)) ? "vertical-rl" : "lr";});

    treemap_labels
        .exit()
        .remove();

    // Add title

    chart_group.append("text")
        .attr("class", "title")
        .attr("id", "chart-title")
        .attr("y", -25)
        .attr("x", chart_width / 2)
        .style("font-weight", "bold")               
        .style("text-anchor", "middle")
        .text("Rental statistics of " + selected_area);
};



/////////////////////////////////////////////////////////////////
//////// EXAMPLE CODE FOR DIFFERENT SHAPE ELEMENTS IN D3 ////////

// var circle = svgContainer.append("circle")
// 						.attr("cx", 30)
// 						.attr("cy", 30)
// 						.attr("r", 20);

//  var rectangle = svgContainer.append("rect")
// 							.attr("x", 10)
// 							.attr("y", 10)
// 							.attr("width", 50)
// 							.attr("height", 100);


// var ellipse = svgContainer.append("ellipse")
// 							.attr("cx", 50)
// 							.attr("cy", 50)
// 							.attr("rx", 25)
// 							.attr("ry", 10)
// 							.attr("fill", "red")
// 							.attr("id", "ellipse");

// d3.select("#ellipse").attr("fill","green");


// var line = svgContainer.append("line")
//                          .attr("x1", 5)
//                          .attr("y1", 5)
//                          .attr("x2", 50)
//                          .attr("y2", 50)
//                          .attr("stroke-width", 2)
//                          .attr("stroke", "blue");

// var arc = d3.arc()
//     .innerRadius(40)
//     .outerRadius(100)
//     .startAngle(0)
//     .endAngle(3);

// svgContainer.append("path")
// 	.attr("transform", "translate(" + 100 + "," + 100 + ")")
//     .attr("d", arc)
//     .attr("fill", "red")
//     .attr("class", "arc")
//     .on("click", function(d) {
//     	d3.select(".arc").attr("fill","blue");
//     });

/////////////////////////////////////////////////////////////////
